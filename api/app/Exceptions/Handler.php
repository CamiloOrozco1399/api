<?php

namespace App\Exceptions;

use Illuminate\Database\Eloquent\ModelNotFoundException;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Throwable;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'current_password',
        'password',
        'password_confirmation',
    ];

    /**
     * Register the exception handling callbacks for the application.
     *
     * @return void
     */
    public function register()
    {
        $this->reportable(function (Throwable $e) {
            //
        });
    }

    //Se utliza validaciones por si se presenta algun error
    public function render($request, throwable $exception)
    {
        if ($exception instanceof ModelNotFoundException) {
            return response()->json(["res" => false, "error" => "Error de modelo"], 400);
        }

        if ($exception instanceof HttpException) {
            return response()->json(["res" => false, "message" => "Error de Ruta"], 404 );
        }

        return parent::render($request, $exception);
    }
}

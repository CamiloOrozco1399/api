<?php

namespace App\Http\Controllers;

use App\Models\Datos;
use App\Http\Requests\CreateDatosRequest;
use App\Http\Requests\UpdateDatosRequest;
use Illuminate\Http\Request;


class DatosController extends Controller
{
    //Get lista
    public function index(Request $request)
    {
       if($request->has('txtBuscar'))
       {
            $dato = Datos::Where('texto','like','%'.$request->txtBuscar.'%')
                                ->orWhere('documento', $request->txtBuscar)
                                ->get();
       }
       else{
           $dato = Datos::all();
       }
        return $dato;
    }

    //Insertar datos por medio de POST
    public function store(CreateDatosRequest $request)
    {
        $input = $request->all();
        Datos::create($input);
        return response()->json([
            'res' => true,
            'message' => 'Se registro correctamente'
        ],   200);
    }

    //Retorna un solo registro
    public function show(Datos $dato)
    {
        return $dato;
    }

    //Se actualizan los datos por medio de PUT
    public function update(UpdateDatosRequest $request, Datos $dato)
    {
        $inputs = $request->all();
        $dato->update($inputs);
        return response()->json([
            'respuesta' => true,
            'message' => 'Se actualizo correctamente'
        ],   200);
        
    }

    //Eliminar registros DELETE
    public function destroy($id)
    {
        Datos::destroy($id);
        return response()->json([
            'respuesta' => true,
            'message' => 'Se elimino correctamente'
        ],   200);
    }
}

<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateDatosRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    //Se agregan validaciones a los campos 
    public function rules()
    {
        return [
            'nombre' => 'min:5|max:100',
            'documento' => 'min:5|max:12'
        ];
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Datos extends Model
{
    use HasFactory;

    //Se crear la tabla donde se van a registrar los datos con sus campos 
    protected $table = 'datos';

    protected $fillable = [
        'texto',
        'documento'
    ];
    //Se ocultan los datos que no necesitamos 
    protected $hidden = [
        'created_at',
        'updated_at'
    ];
}

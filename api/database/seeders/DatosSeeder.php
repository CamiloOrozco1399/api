<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatosSeeder extends Seeder
{
    
    //Se crean datos para registrar en la base de datos
    public function run()
    {
        DB::table('datos')->insert([
            [
                'texto' => 'aqui estoy',
                'documento' => 123456789
            ],
            [
                'texto' => 'halla voy',
                'documento' => 345678932
            ]
        ]);
    }
}
